//
//  Data.swift
//  Task
//
//  Created by mohamed dorgham on 25/12/2021.
//

import Foundation

// MARK: - optionShowsResponseElement
struct ShowsResponse: Codable {
    var score: Double?
    var show: Show?
}

// MARK: - optionShow
struct Show: Codable {
    var id: Int?
    var url: String?
    var name: String?
    var type: Type?
    var language: Language?
    var genres: [String]?
    var status: Status?
    var runtime: Int?
    var averageRuntime: Int?
    var premiered, ended: String?
    var officialSite: String?
    var schedule: Schedule?
    var rating: Rating?
    var weight: Int?
    var network, webChannel: Network?
    var dvdCountry: String?
    var externals: Externals?
    var image: Image?
    var summary: String?
    var updated: Int?
    var _links: Links?

}

// MARK: - optionExternals
struct Externals: Codable {
    var tvrage, thetvdb: Int?
    var imdb: String?
}

// MARK: - optionImage
struct Image: Codable {
    var medium, original: String?
}

enum Language: String, Codable {
    case english = "English"
    case japanese = "Japanese"
}

// MARK: - optionLinks
struct Links: Codable {
    var linksSelf, previousepisode: Previousepisode?

    enum CodingKeys: String, CodingKey {
        case linksSelf = "self"
        case previousepisode
    }
}

// MARK: - optionPreviousepisode
struct Previousepisode: Codable {
    var href: String?
}

// MARK: - optionNetwork
struct Network: Codable {
    var id: Int?
    var name: String?
    var country: Country?
}

// MARK: - optionCountry
struct Country: Codable {
    var name, code, timezone: String?
}

// MARK: - optionRating
struct Rating: Codable {
    var average: Int?
}

// MARK: - optionSchedule
struct Schedule: Codable {
    var time: String?
    var days: [String]?
}

enum Status: String, Codable {
    case ended = "Ended"
}

enum Type: String, Codable {
    case animation = "Animation"
    case documentary = "Documentary"
    case scripted = "Scripted"
}

typealias optionShowsResponse = [ShowsResponse]


class Api {
    
    func getShows(completion: @escaping ([ShowsResponse]) -> ()) {
        guard let url = URL(string: "https://api.tvmaze.com/search/shows?q=Future") else {return}
        
        URLSession.shared.dataTask(with: url) { data, _, _ in
            let shows = try! JSONDecoder().decode([ShowsResponse].self, from: data!)
            
            DispatchQueue.main.async {
                completion(shows)
            }
        }
        .resume()
    }
}
