//
//  ViewController.swift
//  Task
//
//  Created by mohamed dorgham on 26/12/2021.
//

import UIKit
import AdvancedPageControl

class HomeVC: UIViewController {

    @IBOutlet weak var homeCollection: UICollectionView!
    
    var showsArr: [ShowsResponse] = []
    var shows: [Show] = []
    
    static var identifier: String {
        return String(describing: self)
    }
    private lazy var loader : UIView = {
        return createActivityIndicator(self.view)
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getShows()
        homeCollection.register(ImageCell.self, forCellWithReuseIdentifier: ImageCell.identifier)
        homeCollection.register(ImageCell.nib, forCellWithReuseIdentifier: ImageCell.identifier)
        homeCollection.register(ShowCell.self, forCellWithReuseIdentifier: ShowCell.identifier)
        homeCollection.register(ShowCell.nib, forCellWithReuseIdentifier: ShowCell.identifier)
    }

    func getShows() {
        self.loader.isHidden = false
        Api().getShows { (shows) in
            self.showsArr = shows
            for show in self.showsArr {
                self.shows.append(show.show!)
            }
            DispatchQueue.main.async {
                self.loader.isHidden = true
                self.homeCollection.reloadData()
            }
        }
    }

}

