//
//  DetailVC.swift
//  Task
//
//  Created by mohamed dorgham on 26/12/2021.
//

import UIKit
import Cosmos

class DetailVC: UIViewController {

    @IBOutlet weak var imageConstraint: NSLayoutConstraint!
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var showDescription: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var premierdLabel: UILabel!
    @IBOutlet weak var endedLabel: UILabel!
    @IBOutlet weak var genresLabel: UILabel!
    @IBOutlet weak var runTimeLabel: UILabel!
    
    var show: Show?
    static var identifier: String {
        return String(describing: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadShow()
        imageConstraint.constant = (self.view.frame.height * 0.75)
    }
    
    @IBAction func backBtnAction(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    @IBAction func playBtnAction(_ sender: Any) {
        utilities.open(url: show?.url ?? "")
    }
    
    func loadShow() {
        self.showName.text = show?.name ?? ""
        self.premierdLabel.text = show?.premiered ?? ""
        self.endedLabel.text = show?.ended ?? ""
        self.runTimeLabel.text = "\(show?.runtime ?? 0)"
        self.showDescription.text = show?.summary ?? ""
        self.typeLabel.text = show?.type?.rawValue ?? ""
        self.ratingView.rating = 3.5
        self.genresLabel.text = show?.genres?.joined(separator: " | ")
        if let url = URL(string: show?.image?.original ?? "") {
            UIImage.loadFrom(url: url) { image in
                self.showImage.image = image
            }
        }
    }

}
