//
//  ImageCell.swift
//  Aqar
//
//  Created by mohamed dorgham on 21/07/2021.
//

import UIKit
import Cosmos
import AdvancedPageControl


class ImageCell:UICollectionViewCell, FSPagerViewDelegate, FSPagerViewDataSource {
    @IBOutlet weak var pagerWidth: NSLayoutConstraint!

    static var identifier: String {
        return String(describing: self)
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    override init(frame: CGRect) {
         super.init(frame: frame)
         
     }
     
     required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
     }
    @IBOutlet weak var premierdLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var MovieNameLbl: UILabel!
    @IBOutlet weak var pageControl: AdvancedPageControlView!
    
    var shows: [Show] = []{
        didSet {
            self.pageControl.drawer = ExtendedDotDrawer(numberOfPages: shows.count,
                                                                            space: 4.0,
                                                                            indicatorColor: #colorLiteral(red: 0.8669196235, green: 0.009308693405, blue: 0.0944086288, alpha: 1),
                                                                            dotsColor: .lightGray,
                                                                            isBordered: false,
                                                                            borderWidth: 0.0,
                                                                            indicatorBorderColor: .clear,
                                                                            indicatorBorderWidth: 0.0)
            pagerWidth.constant = CGFloat((shows.count) * 30)
            pagerView.reloadData()
        }
    }
    var currentPage: Int = 0 {
        didSet {
            pageControl.setPage(currentPage)
        }
    }
    var isFull: Bool!{
        didSet {
            pagerView.interitemSpacing = CGFloat(8)
            let newScale = 0.25+CGFloat(0.45)*0.25
            pagerView.itemSize = self.pagerView.frame.size.applying(CGAffineTransform(scaleX: newScale, y: newScale))
        }
    }
    func numberOfItems(in pagerView: FSPagerView) -> Int {
        return shows.count
    }
    func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {
        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: "cell", at: index)
        self.currentPage = index
        
        if let url = URL(string: shows[index].image?.original ?? "") {
            UIImage.loadFrom(url: url) { image in
                cell.imageView?.image = image
            }
        }

        self.MovieNameLbl.text = shows[index].name ?? ""
        self.premierdLbl.text = shows[index].premiered ?? ""
        cell.imageView?.contentMode = .scaleToFill
        cell.imageView?.clipsToBounds = true
        return cell
    }
    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }

    @IBOutlet weak var pagerView: FSPagerView!{
        didSet {
            pagerView.delegate = self
            pagerView.dataSource = self
            self.pagerView.register(FSPagerViewCell.self, forCellWithReuseIdentifier: "cell")
            self.pagerView.itemSize = FSPagerView.automaticSize
            pagerView.automaticSlidingInterval = 5
            pagerView.decelerationDistance = 1
            pagerView.backgroundColor = .white
            pagerView.bounces = false
            pagerView.isInfinite = true
            
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
