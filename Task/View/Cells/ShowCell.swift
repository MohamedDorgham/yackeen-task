//
//  ShowCell.swift
//  Task
//
//  Created by mohamed dorgham on 26/12/2021.
//

import UIKit
import Cosmos
import Closures

class ShowCell: UICollectionViewCell {
    
    @IBOutlet weak var showImage: UIImageView!
    @IBOutlet weak var showName: UILabel!
    @IBOutlet weak var premierdLbl: UILabel!
    @IBOutlet weak var runTimeLbl: UILabel!
    @IBOutlet weak var ratingView: CosmosView!
    @IBOutlet weak var openLinkBtn: UIButton!
    
    
    static var identifier: String {
        return String(describing: self)
    }
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        self.layer.cornerRadius = 10

    }
    
    func ConfigureCell(show: Show) {
        
        self.showName.text = show.name
        self.premierdLbl.text = show.premiered
        self.runTimeLbl.text = "\(show.runtime ?? 0)"
        self.ratingView.rating = 3.5
        if let url = URL(string: show.image?.original ?? "") {
            UIImage.loadFrom(url: url) { image in
                self.showImage.image = image
            }
        }
        self.openLinkBtn.onTap {
            utilities.open(url: show.url ?? "")
        }
    }
}
